hohoho
=====

An OTP application presented for the Montrehack 2020 in december.

Build
-----

`docker build -t hohoho:1 .`


Run
-----

`docker run --rm -p 8080:8080 -p 8989:8989 hohoho:1`


Challenge
-----

* Go to `localhost:8080`.
* Get indices on how to connect to erlang vm in website
* Optionally get other indices on how to debug the app if we feel like it
* Connect to the erlang vm: `ssh -p 8989 patate@localhost` password is `patate`
* Remote debug the erlang vm to find out a module named `hohoho_secret` (`io:format("~p~n", [registered()]).` call outputs it among many others)
* `recon:get_state(hohoho_secret).` to get the flag
* do not run `q().` or any other call that would shut down the vm (unless you want to stop the docker container...)

