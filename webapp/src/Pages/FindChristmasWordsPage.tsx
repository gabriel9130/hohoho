import { PageHeader, Input, Button } from "antd";
import React, { useState } from "react";
import useFetch from "react-fetch-hook";
import Navbar from "../Components/NavBar";
import { uuidv4 } from "../Utils/Uuidv";
import imgNoel from "../bebe-bonhomme-neige-message.png";

type FindsChristmasWordsProps = {};

export function FindsChristmasWordsPage(props: FindsChristmasWordsProps) {
  const [text, setText] = useState("");
  const [response, setResponse] = useState("");
  const [idFetch, setIdFetch] = useState("");
  const fetchTransformation = () => setIdFetch(uuidv4());
  const { isLoading, data } = useFetch(`/api/words`, {
    depends: [idFetch],
  });

  return (
    <div className="flexColumn">
      <Navbar />
      <div className="center">
        <PageHeader title="Find christmas words" />
        <img src={imgNoel} width={300} style={{ borderRadius: "6px" }}></img>
        <p>Enter a text and all the christmas words are going to be found.</p>
      </div>
      <div className="flex">
        <div className="w-50 mg-sm">
          <label>Text:</label>
          <Input.TextArea
            size="large"
            maxLength={500}
            onChange={(event) => setText(event.currentTarget.value)}
          />
          <Button
            type="primary"
            htmlType="submit"
            className="w-20"
            style={{ marginTop: "10px" }}
            onClick={() => {
              fetchTransformation();
              setResponse(simulateBackendResponse(text));
            }}
          >
            Submit
          </Button>
        </div>
        <div className="w-50 mg-sm">
          <label>Result:</label>
          <Input.TextArea size="large" value={response} />
        </div>
      </div>
    </div>
  );
}
const CHRISTMAS_WORDS: string[] = [
  "christmas",
  "santa",
  "claus",
  "25",
  "fir",
  "leprechaun",
  "gift",
  "gifts",
];

function simulateBackendResponse(text: string): string {
  return text
    .split(" ")
    .filter((x) => CHRISTMAS_WORDS.includes(x.toLowerCase()))
    .join("\n");
}
